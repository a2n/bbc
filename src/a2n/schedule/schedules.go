package schedule

import (
    "net/http"
    "log"
    "time"
    "fmt"
    "io/ioutil"
    "encoding/json"
    //"reflect"

    "github.com/kuroneko/gosqlite3" 
    "a2n"
)

func Get(station string, date time.Time) {
    //remote(station, date)
    //local(date)
}

func Get2() {
    fromDB()
}

func remote(station string, date time.Time) {
    url := fmt.Sprintf("http://www.bbc.co.uk/%s/programmes/schedules/%d/%d/%d.json", station, date.Year(), date.Month(), date.Day())
    resp, err := http.Get(url)
    a2n.CheckError(err)
    if resp.StatusCode != 200 {
	log.Printf("%d-%d-%d has no data.", date.Year(), date.Month(), date.Day())
	return
    }

    data, err := ioutil.ReadAll(resp.Body)
    _, err = parse(data)
    if err != nil {
	log.Print(err.Error())
	return
    }

    path := fmt.Sprintf("/home/a2n/project/bbc/data/schedules/%d-%d-%d.json", date.Year(), date.Month(), date.Day())
    err = ioutil.WriteFile(path, data, 0600)
    a2n.CheckError(err)
    log.Printf("%d-%d-%d\n", date.Year(), date.Month(), date.Day())
}

func local(date time.Time) {
    url := fmt.Sprintf("../data/schedules/%d-%d-%d.json", date.Year(), date.Month(), date.Day())
    b, err := ioutil.ReadFile(url)
    a2n.CheckError(err)
    _, err = parse(b)
    a2n.CheckError(err)
}

func fromDB() {
    db, err := sqlite3.Open("/home/a2n/project/bbc/src/db")
    defer db.Close()
    a2n.CheckError(err)

    q := "SELECT Content FROM Schedules_raw"
    slice := make([]Output, 0)
    _, err = db.Execute(q, func(st *sqlite3.Statement, values ...interface{}) {
	os, err := parse([]byte(values[0].(string)))
	a2n.CheckError(err)
	slice = append(slice, os...)
    })
    a2n.CheckError(err)

    db.Begin()
    for _, o := range slice {
	no := o.Escape()
	q := fmt.Sprintf("INSERT INTO Programe VALUES ('%s', '%s', '%s', '%d', '%d')", no.Station, no.Title, no.Pid, no.Start, no.End)
	db.Execute(q)
    }
    db.Commit()
}

func parse(data []byte) (outputs []Output, err error) {
    s := Schedules{}
    err = json.Unmarshal(data, &s)

    os := make([]Output, 0)
    for _, v := range s.Schedule.Day.Broadcasts {
	o := Output {
	    s.Schedule.Service.Title,
	    v.Program.DisplayTitle.Title,
	    v.Program.Pid,
	    v.Start.Unix(),
	    v.End.Unix(),
	}
	os = append(os, o)
    }
    return os, err
}
