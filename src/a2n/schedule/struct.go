package schedule

import (
    "time"
)

type Schedules struct {
    Schedule		Schedule `json:"schedule"`
}

type Schedule struct {
    Service	Service `json:"service"`
    Day		Day	`json:"day"`
}

type Service struct {
    Title	string	`json:"title",omitempty`
}

type Day struct {
    Broadcasts	    []Broadcast	    `json:"broadcasts"`
}

type Broadcast struct {
    Start	    time.Time `json:"start"`
    End		    time.Time `json:"end"`
    Program	    Programme `json:"programme"`
}

type Programme struct {
    Pid		    string `json:"pid"`
    DisplayTitle    DisplayTitles `json:"display_titles"`
}

type DisplayTitles struct {
    Title	    string	`json:"title",omitempty`
}

type Output struct {
    Station	    string
    Title	    string
    Pid		    string
    Start	    int64
    End		    int64
}
