package schedule

import (
    "log"
    "fmt"
    "strings"

    "github.com/kuroneko/gosqlite3" 
)

func (o *Output) DB(db *sqlite3.Database) {
    err := db.Begin()
    if err != nil {
	log.Printf("Can not begin.")
	return
    }

    q := fmt.Sprintf("INSERT INTO Programe (Station, Title, Pid, Start, End) VALUES ('%s', '%s', '%s', '%s', '%d', '%d')", o.Station, o.Title, o.Pid, o.Start, o.End)
    _, err = db.Execute(q)
    if err != nil {
	log.Printf("Can not execute query, %s.\n%s\n", err.Error(), q)
    }
}

func (o *Output) Dump() {
    log.Printf("Station: %s\nTitle: %s\nPid: %s\nStart: %s\nEnd: %s\n", o.Station, o.Title, o.Pid, o.Start, o.End)
}

func (o *Output) Escape() (no *Output){
    return &Output {
	strings.Replace(o.Station, "'", "''", -1),
	strings.Replace(o.Title, "'", "''", -1),
	o.Pid,
	o.Start,
	o.End,
    }
}
