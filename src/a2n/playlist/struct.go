package playlist

type SegmentEvents struct {
    Infos []Information	    `json:"segment_events"`
}

type Information struct {
    Position	uint8	    `json:"position"`
    Seg		Segment	    `json:"segment"`
}

type Segment struct {
    Artist	string	    `json:"artist",omitempty`
    Title	string	    `json:"track_title",omitempty`
}

type Record struct {
    Pid			string
    Artist		string
    Title		string
    Position		uint8
}

type Record2 struct {
    Pid	    string
    Content string
}
