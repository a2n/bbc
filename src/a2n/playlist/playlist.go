package playlist

import (
    "io/ioutil"
    "encoding/json"
    "net/http"
    "fmt"
    "strings"
    "log"

    "github.com/kuroneko/gosqlite3"

    "a2n"
)

func Get(pid string) {
    //remote(pid)
    local()
}

func Get2() {
    fromDB()
}

func fromDB() {
    db, err := sqlite3.Open("db")
    defer db.Close()
    a2n.CheckError(err)

    rs := make([]Record2, 0)
    q := "SELECT * FROM Playlist_raw WHERE ORDER BY pid LIMIT 1"
    _, err = db.Execute(q, func(st *sqlite3.Statement, values ...interface{}) {
	rs = append(rs, Record2 {
	    values[0].(string),
	    values[1].(string),
	})
    })

    for _, v := range rs {
	log.Printf("Pid: %s\n, Content: %d\n", v.Pid, len(v.Content))
    }
}

func remote(pid string) string {
    url := fmt.Sprintf("http://212.58.246.95/programmes/%s/segments.json", pid)
    resp, err := http.Get(url)
    if err != nil {
	log.Printf("%d remote has error, %s\n", pid, err.Error())
	return ""
    }

    if resp.StatusCode != 200 {
	log.Printf("%s has no data.", pid)
	return ""
    }

    data, err := ioutil.ReadAll(resp.Body)
    a2n.CheckError(err)
    return string(data)
}

func local() {
    const path = "../data/playlist.json"
    bs, err := ioutil.ReadFile(path)
    a2n.CheckError(err)
    rs, err := parse(bs)
    a2n.CheckError(err)
    escapeQuote(rs)
    //dump(rs)
    //savedb(rs)
}

func dump(rs *[]Record) {
    newRS := escapeQuote(rs)
    for _, v := range *newRS {
	fmt.Printf("%s, %s, %d\n", v.Artist, v.Title, v.Position)
    }
}

func parse(data []byte) (*[]Record, error) {
    s := SegmentEvents{}
    err := json.Unmarshal(data, &s)
    if err != nil {
	return nil, err
    }

    rs := make([]Record, 0)
    for _, v := range s.Infos {
	record := Record {
	    "",
	    v.Seg.Artist,
	    v.Seg.Title,
	    v.Position,
	}
	rs = append(rs, record)
    }
    return &rs, nil
}

func escapeQuote(rs *[]Record) (*[]Record) {
    newRS := make([]Record, 0)

    for _, v := range *rs {
	artist := strings.Replace(v.Artist, "'", "''", -1)
	title := strings.Replace(v.Title, "'", "''", -1)
	fmt.Printf("%s, %s, %d\n", artist, title, v.Position)
	r := Record {
	    "",
	    artist,
	    title,
	    v.Position,
	}
	newRS = append(newRS, r)
    }
    return &newRS
}

func savedb(rs *[]Record) {
    newRS := escapeQuote(rs)

    db, err := sqlite3.Open("/home/a2n/project/bbc/src/db")
    defer db.Close()
    a2n.CheckError(err)

    err = db.Begin()
    if err != nil {
	log.Printf("Can not begin, %s\n", err.Error())
	return
    }

    for _, v := range *newRS {
	q := fmt.Sprintf("INSERT INTO Playlist (Artist, Title, Position) VALUES ('%s', '%s', %d)", v.Artist, v.Title, v.Position)
	_, err := db.Execute(q)
	if err != nil {
	    log.Printf("Can not execute query, %s\n%s\n", err.Error(), q)
	    continue
	}
    }

    err = db.Commit()
    if err != nil {
	log.Printf("Can not commit.")
	return
    }
}
