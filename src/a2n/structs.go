package a2n

type SegmentEvents  struct {
    Events	*[]SegmentEvent
}

type SegmentEvent struct {
    Position		int
    Pid			string
    Seg			Segment
}

type Segment struct {
    Artist	    string
    Title	    string
    Publisher	    string
}
