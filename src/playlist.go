package main

import (
    "fmt"
    "io/ioutil"
    "path/filepath"
    "encoding/xml"
    "os"
    //"log"

    "code.google.com/p/gosqlite/sqlite"
)

func main() {
    f1()
}

func f1() {
    scs := readSchedules()
    for _, v := range *scs {
	v.readPlaylists()
    }
    /*
    ps := make([]playlist, 0)
    for _, v := range ss {
	ps = append(ps, p)
    }

    write(ps)
    */
}

type segment_events struct {
    XMLName	    xml.Name	`xml:"segment_events"`
    ses		    []segment_event	`xml:"segment_event"`
}

type segment_event struct {
    segment	    playlist	`xml:"segment"`
}

type playlist struct {
    XMLName	    xml.Name	`xml:"segment"`
    Type	    string	`xml:"type,attr"`
    artist	    string	`xml:"artist"`
    title	    string	`xml:"title"`
    position	    int		`xml:"position"`
    duration	    int		`xml:"duration"`
    pid		    string
    played_date	    int
    release_date    int
}

func (sc *schedule) readPlaylists() {
    const PATH = "/home/a2n/projects/go/bbc/data/playlist"
    path := filepath.Join(PATH, sc.PID)
    b, e := ioutil.ReadFile(path)
    if os.IsNotExist(e) {
	//log.Printf("%s is not exist.\n", sc.PID)
	return
    }

    var ses segment_events
    e = xml.Unmarshal(b, &ses)
    cherr(e)
    fmt.Printf("%v\n", ses.ses[0].segment.artist)
}

func write(ps *[]playlist) {
}

type schedule struct {
    PID		string
    Start	int
    End		int
}

func readSchedules() (*[]schedule) {
    c, e := sqlite.Open("db")
    cherr(e)

    q := "SELECT pid, start, end FROM Programe;"
    s, e := c.Prepare(q)
    e = s.Exec()
    cherr(e)

    scs := make([]schedule, 0)
    for s.Next() {
	var sc schedule
	e = s.Scan(&sc.PID, &sc.Start, &sc.End)
	cherr(e)
	scs = append(scs, sc)
    }
    return &scs
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}
