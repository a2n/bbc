package main

import (
    "fmt"

    "a2n"
    "github.com/kuroneko/gosqlite3"
)

func main() {
    db, err := sqlite3.Open("db")
    defer db.Close()
    if err != nil {
	panic(err)
    }

    q := "SELECT * FROM schedules_raw LIMIT 1"
    _, err = db.Execute(q, func(st *sqlite3.Statement, values...interface{}) {
	fmt.Printf("len: %d\n", len(values))
	fmt.Printf("%s\n", values[0])
    })
    a2n.CheckError(err)

}
