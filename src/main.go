package main

import (
    "time"

    "a2n"
    "a2n/schedule"
    "a2n/playlist"
)

func main() {
    playlist.Get2()
}

func p1() {
    playlist.Get("")
}

func s2() {
    d1 := time.Date(2011, time.July, 31, 0, 0, 0, 0, time.UTC)
    schedule.Get(a2n.Station1xtra, d1)
}

func s1() {
    d1 := time.Date(2007, time.September, 15, 0, 0, 0, 0, time.UTC)
    d2 := time.Date(2013, time.May, 1, 0, 0, 0, 0, time.UTC)
    max := int((d2.Unix() - d1.Unix()) / 86400)

    for i := 0; i < max; i++ {
	schedule.Get(a2n.Station1xtra, d1)
	d1 = d1.AddDate(0, 0, 1)
    }
}
