package main

import (
    "io/ioutil"
    "fmt"
    "strings"

    "a2n"
    "github.com/kuroneko/gosqlite3"
)

func main() {
    foo()
}

const (
    dataPath = "/home/a2n/project/bbc/data/programes/"
)

type Raw struct {
    Pid	    string
    Content string
}

func foo() {
    db, err := sqlite3.Open("db")
    a2n.CheckError(err)
    defer db.Close()

    err = db.Begin()
    a2n.CheckError(err)

    fs, err := ioutil.ReadDir(dataPath)
    a2n.CheckError(err)

    for _, v := range fs {
	content, err := ioutil.ReadFile("../data/programes/" + v.Name())
	str := string(content)
	a2n.CheckError(err)
	q := fmt.Sprintf("INSERT INTO Playlist_raw VALUES ('%s', '%s')", v.Name(), strings.Replace(str, "'", "''", -1))
	db.Execute(q)
    }

    err = db.Commit()
    a2n.CheckError(err)
}
