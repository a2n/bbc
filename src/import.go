package main

import (
    "io/ioutil"
    "a2n"
    "fmt"
    "path/filepath"
    "strings"

    "github.com/kuroneko/gosqlite3"
)

func main() {
    foo()
}

const (
    dataPath =		"../data/schedules"
)

type Record struct {
    Station	    string
    Year	    int
    Month	    int
    Day		    int
    Content	    string
}

func foo() {
    fs, err := ioutil.ReadDir(dataPath)
    a2n.CheckError(err)

    rs := make([]Record, 0)
    for _, v := range fs {
	rs = append(rs, parse(v.Name()))
    }
    write(rs)
}

func parse(name string) (Record) {
    station := "1xtra"
    var year, month, day int
    _, err := fmt.Sscanf(name, "%d-%d-%d.json", &year, &month, &day)
    a2n.CheckError(err)
    content, err := ioutil.ReadFile(filepath.Join(dataPath, name))
    a2n.CheckError(err)
    return Record {
	station,
	year,
	month,
	day,
	string(content),
    }
}

func write(rs []Record) {
    db, err := sqlite3.Open("/home/a2n/project/bbc/src/db")
    a2n.CheckError(err)
    defer db.Close()

    err = db.Begin()
    a2n.CheckError(err)

    for _, v := range rs {
	q := fmt.Sprintf("INSERT INTO Schedules_raw VALUES ('%s', '%d', '%d', '%d', '%s')", v.Station, v.Year, v.Month, v.Day, strings.Replace(v.Content, "'", "''", -1))
	db.Execute(q)
    }

    err = db.Commit()
    a2n.CheckError(err)
}
