var $ = require('jQuery');
var tracks = [];

local();
$.each(tracks, function(k, v) {
    var query = v.Artist + ' ' + v.Title;
    console.log(query);
    search(query);
});

function local() {
    var fs = require('fs');
    var raw = fs.readFileSync('list').toString();
    var jquery = fs.readFileSync('jquery.min.js').toString();

    $(raw).find('.track').each(function(k, v) {
	var artist = $(v).find('.artist').text();
	var title = $(v).find('.title').text();
	if (artist.length > 0 && title.length > 0) {
	    tracks.push(new Object({
		Artist: artist,
		Title: title,
	    }));
	}
    });
}

function search(query) {
    const ApiKey = 'AIzaSyD4a9_JANT7shMEpMw28SCbkTfPBd8zau4';
    var g = require('googleapis');
    g.discover('youtube', 'v3')
    .execute(function(err, client) {
	if (err) {
	    throw err;
	}

	var params = {
	    q: query,
	    part: 'id',
	    key: ApiKey,
	    maxResults: 1,
	    type: 'video',
	    videoDefinition: 'standard',
	    videoDuration: 'short',
	    videoSyndicated: 'true',
	};

	var req = client.youtube.search.list(params);
	req.execute(function(err, resp) {
	    if (err) {
		console.log('execute has error.');
		throw err;
	    }
	    console.log('Got result.');
	    append(resp.items[0].id.videoId + '\n');
	});
    });
}

function append(yid) {
    var fs = require('fs');
    fs.appendFile('list.txt', yid, function(err) {
	if (err) {
	    throw err;
	}

	console.log('Done append.');
    });
}
